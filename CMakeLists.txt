cmake_minimum_required(VERSION 3.8.2)

project(ficial_gate)

set(SRC
    ui.c
    shadow_display.c
    main.c
)

include_directories(${DRM_HEADER_DIR})

add_executable(ficial_gate ${SRC})

set(LIB rkfacial rga pthread ts minigui_ths png12 jpeg freetype)

target_link_libraries(ficial_gate ${LIB})

install(TARGETS ficial_gate DESTINATION bin)

install(FILES minigui/MiniGUI-1280x720.cfg DESTINATION ../etc RENAME MiniGUI.cfg)
install(DIRECTORY minigui DESTINATION local/share)

install(FILES S06_ficial_gate DESTINATION ../etc/init.d
    PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE
    GROUP_READ GROUP_WRITE GROUP_EXECUTE
    WORLD_READ WORLD_WRITE WORLD_EXECUTE)
